import { lorem } from '../src/lorem'

it('returns random capitalized word', () => {
  expect(lorem()).toMatch(/[A-Z][a-z]*/)
})

it('returns requested number of random words', () => {
  expect(lorem(1).split(' ').length).toBe(1)
  expect(lorem(2).split(' ').length).toBe(2)
  expect(lorem(3).split(' ').length).toBe(3)
  expect(lorem(4).split(' ').length).toBe(4)
  expect(lorem(5).split(' ').length).toBe(5)
})
