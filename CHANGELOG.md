# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://gitlab.com/koendirkvanesterik/utils/compare/v0.0.4...v0.0.5) (2020-08-14)

### Features

- introduce lorem utils ([76601be](https://gitlab.com/koendirkvanesterik/utils/commit/76601be399b9101c1cb013a2f25b9f010ae81bba))

### 0.0.4 (2020-04-13)

### Features

- **url:** add concat slashes function to url utils ([16e132b](https://gitlab.com/koendirkvanesterik/utils/commit/16e132b1cd1b5d163deb5fe4d3fb1c4a3e3a0b08))

### 0.0.3 (2020-04-13)

### Features

- **url:** add concat slashes function to url utils ([16e132b](https://gitlab.com/koendirkvanesterik/utils/commit/16e132b1cd1b5d163deb5fe4d3fb1c4a3e3a0b08))

### 0.0.2 (2020-04-12)

### 0.0.1 (2020-04-12)
