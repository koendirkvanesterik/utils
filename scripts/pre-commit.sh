#!/usr/bin/env bash

cd "$(dirname "$0")/.."
set -e

# Unfortunately, this seems to be the only way to disable pre-commit hooks during a rebase. (--no-verify doesn't work)
if [[ $DISABLE_PRECOMMIT_HOOK ]]; then
  echo -e '\x1B[31mNOTE: DISABLE_PRECOMMIT_HOOK is set, pre-commit hook is disabled.\x1B[0m';
  exit 0;
else
  echo -e '\x1B[90mTo disable the pre-commit hook during a rebase, use: `DISABLE_PRECOMMIT_HOOK=true git rebase ...`\x1B[0m'
fi

npx concurrently \
  --kill-others-on-fail \
  "npm run --silent check:package-lock"
  "lint-staged" \
  "npm run --silent test:types"
