#!/usr/bin/env bash

cd "$(dirname "$0")/.."
set -e

if grep '"resolved": "http:' package-lock.json; then
  echo -e '\n\x1B[31mERROR: The package-lock.json file contains http urls.\nPlease convert these to https urls\x1B[0m\n\n';
  exit 1;
fi
